﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StupidRush_Server
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Thread thread;
        public delegate void UpdateStatusTextCallBack(String status);
        private StupidRushEntities entities = new StupidRushEntities();

        private KategoriController kc = new KategoriController();
        private SubKategoriController skc = new SubKategoriController();
        private SoalController sc = new SoalController();
        private JawabanController jc = new JawabanController();
        private int index = 1;
        private ServerHandler handler;

        bool statusAdd = true;

        public void AddStatus(String status)
        {
            txtbox_Status.Dispatcher.Invoke(new UpdateStatusTextCallBack(UpdateText), status);
        }

        private void UpdateText(String status)
        {
            String stm;
            using (StreamWriter file = File.AppendText(@"log1.log"))
            {
                file.WriteLine(status);
            }
            if (statusAdd) txtbox_Status.Text += "\n" + status;
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            AddStatus("Server initialized...");
            txtBox_Command.Focus();

            handler = new ServerHandler(this);

            thread = new Thread(new ThreadStart(handler.Start));
            thread.Start();

            MatchContainer.setParent(this);

            Show();
        }

        private void Show()
        {
            grid1.ItemsSource = kc.GetAllData().ToList().Select(r => new { r.IDKategori, r.Kategori1 }).ToList();
            gridSubKategori.ItemsSource = skc.GetAllData().ToList().Select(r => new { r.IDKategori, r.IDSubKategori, r.SubKategori1}).ToList();
            gridSoal.ItemsSource = sc.GetAllData().ToList().Select(r => new { r.IDSoal, r.IDSubKategori, r.Soal1 }).ToList();
            ComboBoxSubCategory.ItemsSource = kc.GetAllData().ToList();
            ComboBoxSubCategory.DisplayMemberPath = "Kategori1";
            ComboBoxSoal.ItemsSource = skc.GetAllData().ToList();
            ComboBoxSoal.DisplayMemberPath = "SubKategori1";
            ComboBoxJawabanSubCategory.ItemsSource = skc.GetAllData().ToList();
            ComboBoxJawabanSubCategory.DisplayMemberPath = "SubKategori1";
            ComboBoxJawaban1.Items.Clear();
            ComboBoxJawaban1.Items.Add(0);
            ComboBoxJawaban1.Items.Add(1);
            ComboBoxJawaban1.SelectedValue = 0;
            ComboBoxJawaban2.Items.Clear();
            ComboBoxJawaban2.Items.Add(0);
            ComboBoxJawaban2.Items.Add(1);
            ComboBoxJawaban2.SelectedValue = 0;
            ComboBoxJawaban3.Items.Clear();
            ComboBoxJawaban3.Items.Add(0);
            ComboBoxJawaban3.Items.Add(1);
            ComboBoxJawaban3.SelectedValue = 0;
            ComboBoxJawaban4.Items.Clear();
            ComboBoxJawaban4.Items.Add(0);
            ComboBoxJawaban4.Items.Add(1);
            ComboBoxJawaban4.SelectedValue = 0;
        }

        private void Commands()
        {
            if (txtBox_Command.Text.Equals("pause"))
            {
                statusAdd = !statusAdd;
            }

            txtBox_Command.Text = "";
        }

        private void btn_Submit_Click(object sender, RoutedEventArgs e)
        {
            Commands();
        }

        private void tab_ServerStatus_GotFocus(object sender, RoutedEventArgs e)
        {
            txtBox_Command.Focus();
        }
        
        //Kategori add
        private void CategoryButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                int result = (int)(entities.GetNewKategoriID().FirstOrDefault());
                KategoriController kc = new KategoriController();
                if (result > -1)
                {
                    Kategori k = new Kategori()
                    {
                        IDKategori=result,
                        Kategori1= CategoryTextBox.Text.ToString()
                    };
                    kc.Add(k);
                }
                Show();
            }
        }

        //Kategori remove
        private void CategoryButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                String i = (grid1.SelectedCells[0].Column.GetCellContent(grid1.SelectedItem) as TextBlock).Text;
                kc.Delete(Int32.Parse(i));
                Show();
            }
        }

        //SubKategori add
        private void SubKategoriButtonAdd_Click(object sender, RoutedEventArgs e)
        {
           using (var entities = new StupidRushEntities())
            {
                int result = (int)(entities.GetNewSubKategoriID().FirstOrDefault());
                SubKategoriController skc = new SubKategoriController();
                int i = ComboBoxSubCategory.SelectedIndex;
                if (result > -1)
                {
                    SubKategori sk = new SubKategori()
                    {
                        IDSubKategori = result,
                        IDKategori = (int)ComboBoxSubCategory.SelectedValue,
                        SubKategori1 = SubCategoryTextBox.Text.ToString()
                    };
                    skc.Add(sk);
                }
                Show();
            }
        }

        //SubKategori remove
        private void SubKategoriButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                String i = (gridSubKategori.SelectedCells[1].Column.GetCellContent(gridSubKategori.SelectedItem) as TextBlock).Text;
                skc.Delete(Int32.Parse(i));
                Show();
            }
        }

        private void SoalButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                int result = (int)(entities.GetNewSoalID().FirstOrDefault());
                SoalController s = new SoalController();
                if (result > -1)
                {
                    Soal snew = new Soal()
                    {
                        IDSoal = result,
                        IDSubKategori = (int)ComboBoxSoal.SelectedValue,
                        Soal1 = SoalTextBox.Text.ToString()
                    };
                    s.Add(snew);
                }
                SoalTextBox.Text = "";
                Show();
            }
        }

        private void SoalButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                String i = (gridSoal.SelectedCells[0].Column.GetCellContent(gridSoal.SelectedItem) as TextBlock).Text;
                sc.Delete(Int32.Parse(i));
                Show();
            }
        }

        private void JawabanButtonAdd_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                int result = (int)(entities.GetNewJawabanID().FirstOrDefault());
                JawabanController jc = new JawabanController();
                int idSoal=(int)ComboBoxJawaban.SelectedValue;
                int status1 = (int)ComboBoxJawaban1.SelectedValue;
                int status2 = (int)ComboBoxJawaban2.SelectedValue;
                int status3 = (int)ComboBoxJawaban3.SelectedValue;
                int status4 = (int)ComboBoxJawaban4.SelectedValue;
                string jawaban1 = JawabanTextBox1.Text.ToString();
                string jawaban2 = JawabanTextBox2.Text.ToString();
                string jawaban3 = JawabanTextBox3.Text.ToString();
                string jawaban4 = JawabanTextBox4.Text.ToString();
                if (result > -1)
                {
                    if (!jawaban1.Equals(""))
                    {
                        Jawaban j1 = new Jawaban()
                        {
                            IDJawaban = result,
                            IDSoal = idSoal,
                            Jawaban1 = jawaban1,
                            Status = status1
                        };
                        jc.Add(j1);
                    }

                    if (!jawaban2.Equals(""))
                    {
                        result = (int)(entities.GetNewJawabanID().FirstOrDefault());
                        Jawaban j2 = new Jawaban()
                        {
                            IDJawaban = result,
                            IDSoal = idSoal,
                            Jawaban1 = jawaban2,
                            Status = status2
                        };
                        jc.Add(j2);
                        
                    }
                    if (!jawaban3.Equals(""))
                    {
                        result = (int)(entities.GetNewJawabanID().FirstOrDefault());
                        Jawaban j3 = new Jawaban()
                        {
                            IDJawaban = result,
                            IDSoal = idSoal,
                            Jawaban1 = jawaban3,
                            Status = status3
                        };
                        jc.Add(j3);
                    }
                    if (!jawaban4.Equals(""))
                    {
                        result = (int)(entities.GetNewJawabanID().FirstOrDefault());
                        Jawaban j4 = new Jawaban()
                        {
                            IDJawaban = result,
                            IDSoal = idSoal,
                            Jawaban1 = jawaban4,
                            Status = status4
                        };
                        jc.Add(j4);
                    }
                }
                Show();
                gridJawaban.ItemsSource = jc.GetAllData(idSoal).ToList().Select(r => new { r.IDSoal, r.IDJawaban, r.Jawaban1, r.Status }).ToList();
                JawabanTextBox1.Text = "";
                JawabanTextBox2.Text = "";
                JawabanTextBox3.Text = "";
                JawabanTextBox4.Text = "";
            }
        }

        private void JawabanButtonRemove_Click(object sender, RoutedEventArgs e)
        {
            using (var entities = new StupidRushEntities())
            {
                String i = (gridJawaban.SelectedCells[1].Column.GetCellContent(gridJawaban.SelectedItem) as TextBlock).Text;
                jc.Delete(Int32.Parse(i));
                gridJawaban.ItemsSource = jc.GetAllData((int)ComboBoxJawaban.SelectedValue).ToList().Select(r => new { r.IDSoal, r.IDJawaban, r.Jawaban1, r.Status }).ToList();
                Show();
            }

        }

        private void ComboBoxJawaban_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                gridJawaban.ItemsSource = jc.GetAllData((int)ComboBoxJawaban.SelectedValue).ToList().Select(r => new { r.IDSoal, r.IDJawaban, r.Jawaban1, r.Status }).ToList();
            }
            catch (Exception a)
            {
            }
        }

        private void ComboBoxJawabanSubCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBoxJawaban.ItemsSource = sc.GetDataBySubCategory((int)ComboBoxJawabanSubCategory.SelectedValue).ToList();
            ComboBoxJawaban.DisplayMemberPath = "Soal1";  
        }

        private void txtBox_Command_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Commands();
            }
        }

        private void Window_Closing_1(object sender, System.ComponentModel.CancelEventArgs e)
        {
            handler.Listening = false;

            handler = null;
            thread = null;
        }
    }
}
