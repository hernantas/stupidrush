﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush
{
    class RequestMessage
    {
        public delegate void ResponseHandler(String message);

        public ResponseHandler Handler { set; get; }
        public String Request { set; get; }
        public String Response { set; get; }
        public bool IsResponded { set; get; }

        public RequestMessage(String message, ResponseHandler handler)
        {
            Response = "";
            Request = message;
            Handler = handler;

            IsResponded = false;
        }
    }
}
