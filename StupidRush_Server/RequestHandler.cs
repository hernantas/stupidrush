﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StupidRush_Server.Controllers;
 
namespace StupidRush_Server
{
    public class RequestHandler
    {
        protected String requestType;
        public String RequestType { get { return requestType; } }
        public Dictionary<String, String> Response { set; get;} 
        public Dictionary<String, String> Request { set; get; }
        public Dictionary<String, String> Session { set; get; }
        protected StupidRushEntities entities = new StupidRushEntities();
        protected MainWindow parent;
        protected SkorController skrc = new SkorController();
        protected UserController uc = new UserController();
        protected SoalController sc = new SoalController();
        protected JawabanController jc = new JawabanController();
        protected KategoriController kc = new KategoriController();
        protected SubKategoriController skc = new SubKategoriController();

        public void Clear()
        {
            Response = new Dictionary<string, string>();
            Request = new Dictionary<string, string>();
            Session = new Dictionary<string, string>();
        }

        public void setParent(MainWindow window)
        {
            parent = window;
        }

        protected bool checkAuth()
        {
            string userName = Session["username"];
            string passWord = Session["password"];
            User find = (from p in entities.Users
                         where p.Username.Equals(userName) && p.Password.Equals(passWord)
                         select p).FirstOrDefault();
            if (find == null) return false;
            return true;
        }

        public virtual void onReceiveRequest()
        {
        }
    }
}
