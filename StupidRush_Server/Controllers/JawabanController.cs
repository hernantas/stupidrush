﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StupidRush_Server
{
    public class JawabanController
    {
        StupidRushEntities entities = new StupidRushEntities();

        public List<Jawaban> GetJawabanCorrect(int IDSoal)
        {
            List<Jawaban> jawabanCorrect = (from p in entities.Jawabans
                                            where p.IDSoal == IDSoal && p.Status == 1
                                            select p).ToList();

            return jawabanCorrect;
        }

        public List<Jawaban> GetJawabanWrong(int IDSoal)
        {
            List<Jawaban> jawabanWrong = (from p in entities.Jawabans
                                            where p.IDSoal == IDSoal && p.Status == 0
                                            select p).ToList();

            return jawabanWrong;
        }

        public IEnumerable<Jawaban> GetAllData(int ID)
        {
            IEnumerable<Jawaban> results = from b in entities.Jawabans
                                           where b.IDSoal==ID 
                                        select b;
            return results;
        }

        public void Add(Jawaban j)
        {
            try
            {
                entities.Jawabans.Add(j);
                entities.SaveChanges();
                MessageBox.Show("Insert success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Insert failed!");
            }
        }

        public void Delete(int jawabanId)
        {
            try
            {
                Jawaban jawabanToDelete = entities.Jawabans.Find(jawabanId);
                if (jawabanToDelete != null)
                {
                    entities.Jawabans.Remove(jawabanToDelete);
                    entities.SaveChanges();
                    MessageBox.Show("Delete success!");
                }
                else
                {
                    MessageBox.Show("Nothing to delete!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Delete failed!");
            }
        }

    }
}
