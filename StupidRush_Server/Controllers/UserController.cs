﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Controllers
{
    public class UserController
    {
        StupidRushEntities entities = new StupidRushEntities();

        public User GetUser(string userName, string pass)
        {
            User results = (from c in entities.Users
                            where c.Username.Equals(userName) && c.Password.Equals(pass)
                            select c).FirstOrDefault<User>();
            return results;
        }

        public bool find(string username, string password)
        {
            bool find = entities.Users.Any( c => c.Username.Equals(username) && c.Password.Equals(password));
            return find;
        }

        public User Find(string username, string password)
        {
            User find = (from p in entities.Users
                         where p.Username.Equals(username) && p.Password.Equals(password)
                         select p).FirstOrDefault();
            return find;
        }

        public void Add(User j)
        {
            try
            {
                entities.Users.Add(j);
                entities.SaveChanges();
                //MessageBox.Show("Insert success!");
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Insert failed!");
            }
        }

        public void UpdateSkor(string user, string pass, int skorSubCat)
        {
            try
            {
                User userToUpdate = (from s in entities.Users
                                     where s.Username.Equals(user) && s.Password.Equals(pass)
                                     select s).FirstOrDefault();
                if (userToUpdate != null)
                {
                    userToUpdate.TotalSkor += skorSubCat;
                    entities.SaveChanges();
                }
            }
            catch
            {
            }
        }
        public List<User> Rank()
        {

            List<User> rank = (from p in entities.Users
                               orderby p.TotalSkor descending
                               select p).ToList();

            return rank;
        }

        public int Level(string username)
        {
            int level = (int)(from p in entities.Users
                         where p.Username.Equals(username)
                         select p.TotalSkor).FirstOrDefault();

            return level/100;
        }

    }
}
