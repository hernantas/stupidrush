﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StupidRush_Server.Controllers
{
    public class SkorController
    {
        StupidRushEntities entities = new StupidRushEntities();

        public Skor GetSkor(string userName, int IDSub)
        {
            Skor results = (from c in entities.Skors
                            where c.Username.Equals(userName) && c.IDSubKategori==IDSub
                            select c).FirstOrDefault<Skor>();
            return results;
        }

        public void Add(Skor j)
        {
            try
            {
                entities.Skors.Add(j);
                entities.SaveChanges();
                //MessageBox.Show("Insert success!");
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Insert failed!");
            }
        }

        public void Update(Skor b, int skor)
        {
            try
            {
                Skor skorToUpdate = (from s in entities.Skors
                                     where s.IDSkor == b.IDSkor
                                     select s).FirstOrDefault();
                if (skorToUpdate != null)
                {
                    skorToUpdate.IDSkor = b.IDSkor;
                    skorToUpdate.IDSubKategori = b.IDSubKategori;
                    skorToUpdate.Username = b.Username;
                    skorToUpdate.Skor1 += skor;
                    entities.SaveChanges();
                }
            }
            catch
            {
            }
        }
        public void tambahMenang(Skor b)
        {
            try
            {
                Skor skorToUpdate = (from s in entities.Skors
                                     where s.IDSkor == b.IDSkor
                                     select s).FirstOrDefault();
                if (skorToUpdate != null)
                {
                    skorToUpdate.JumlahMenang += 1;
                    entities.SaveChanges();
                }
            }
            catch
            {
            }
        }

        public int FindUser(string userName)
        {
            int results = (from c in entities.Skors
                            where c.Username.Equals(userName)
                            select c).Count();
            return results;
        }

        public int JumlahMenang(string userName, int IDSubKat)
        {
            int results = 0;
            Skor skors = (from c in entities.Skors
                           where c.Username.Equals(userName) && c.IDSubKategori ==  IDSubKat
                           select c).FirstOrDefault();
            if(skors!=null){
             results = (int)skors.JumlahMenang;
            }
            return results;
        }

        public int JumlahMenang(string userName)
        {
            int results = (from c in entities.Skors
                           where c.Username.Equals(userName)
                           select c).AsEnumerable().Sum(dr => dr.JumlahMenang);

            return results;
        }
        public int skorPerSubKategori(string userName, int IDSubKat)
        {
            int results = (from a in entities.Skors
                           where a.Username.Equals(userName) && a.IDSubKategori == IDSubKat
                           select a.Skor1).FirstOrDefault();
            return results;
        }

    }
}
