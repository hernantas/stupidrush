﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StupidRush_Server
{
    public class SoalController
    {
        StupidRushEntities entities = new StupidRushEntities();
        
        public List<Soal> GetAllData(int idSubKat)
        {
            List<Soal> results = (from b in entities.Soals
                                  where b.IDSubKategori == idSubKat
                                  select b).ToList();
            return results;
        }

        public IEnumerable<Soal> GetAllData()
        {
            IEnumerable<Soal> results = from b in entities.Soals
                                               select b;
            return results;
        }
        public IEnumerable<Soal> GetDataBySubCategory(int IDSubCategory)
        {
            IEnumerable<Soal> results = from b in entities.Soals
                                        where b.IDSubKategori==IDSubCategory
                                        select b;
            return results;
        }
        public void Add(Soal s)
        {
            try
            {
                entities.Soals.Add(s);
                entities.SaveChanges();
                MessageBox.Show("Insert success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Insert failed!");
            }
        }
        public void Delete(int soalId)
        {
            try
            {
                Soal soalToDelete = entities.Soals.Find(soalId);
                if (soalToDelete != null)
                {
                    entities.Soals.Remove(soalToDelete);
                    entities.SaveChanges();
                    MessageBox.Show("Delete success!");
                }
                else
                {
                    MessageBox.Show("Nothing to delete!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Delete failed!");
            }
        }
    }
}
