﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using StupidRush_Server.Controllers;
namespace StupidRush_Server
{
   
    public class SubKategoriController
    {
        StupidRushEntities entities = new StupidRushEntities();
        private JawabanController jc = new JawabanController();
        public List<SubKategori> GetAllData(int idKat)
        {
             List < SubKategori > skat = (from p in entities.SubKategoris
                                            where p.IDKategori == idKat &&  p.Soals.Count >= 7
                                            select p).ToList();
            return skat;
        }

        public SubKategori getSubKategori(string subKat)
        {
            SubKategori subKategori = (from p in entities.SubKategoris
                                       where p.SubKategori1.Equals(subKat)
                                       select p).FirstOrDefault();
            return subKategori;
        }

        public IEnumerable<SubKategori> GetAllData()
        {
            IEnumerable<SubKategori> results = from b in entities.SubKategoris
                                               where b.Soals.Count >= 7
                                            select b;
            return results;
        }

        public void Add(SubKategori sk)
        {
            try
            {
                entities.SubKategoris.Add(sk);
                entities.SaveChanges();
                MessageBox.Show("Insert success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Insert failed!");
            }
        }

        public void Delete(int skId)
        {
            try
            {
                SubKategori skToDelete = entities.SubKategoris.Find(skId);
                if (skToDelete != null)
                {
                    entities.SubKategoris.Remove(skToDelete);
                    entities.SaveChanges();
                    MessageBox.Show("Delete success!");
                }
                else
                {
                    MessageBox.Show("Nothing to delete!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Delete failed!");
            }
        }

        public int cekJumlah(int KatID)
        {
            return (from a in entities.SubKategoris
                    where a.IDKategori == KatID
                    select a).Count();
        }
    }
}
