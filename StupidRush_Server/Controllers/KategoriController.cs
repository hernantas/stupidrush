﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
namespace StupidRush_Server
{
    public class KategoriController
    {
        StupidRushEntities entities = new StupidRushEntities();

        private SoalController sc = new SoalController();
        private JawabanController jc = new JawabanController();
        private SubKategoriController skc = new SubKategoriController();

        public int getIDKategori(string kategori)
        {
            int idKat = (from p in entities.Kategoris                    
                         where p.Kategori1.Equals(kategori)
                         select p).FirstOrDefault().IDKategori;

            if (skc.cekJumlah(idKat) >= 1) return idKat;
            else return 0;
        }

        public List<Kategori> GetCategory()
        {
            List<Kategori> results = (from b in entities.Kategoris
                                      where b.SubKategoris.Count >=1
                                            select b).ToList();
            return results;
        }

        public IEnumerable<Kategori> GetAllData()
        {
            IEnumerable<Kategori> results = from b in entities.Kategoris
                                            select b;
            return results;
        }
        public void Add(Kategori kategori)
        {
            try
            {
                entities.Kategoris.Add(kategori);
                entities.SaveChanges();
                MessageBox.Show("Insert success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Insert failed!");
            }
        }
        public void Delete(int kategoriId)
        {
            try
            {
                Kategori kategoriToDelete = entities.Kategoris.Find(kategoriId);
                if (kategoriToDelete != null)
                {
                    entities.Kategoris.Remove(kategoriToDelete);
                    entities.SaveChanges();
                    MessageBox.Show("Delete success!");
                }
                else
                {
                    MessageBox.Show("Nothing to delete!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Delete failed!");
            }
        }

        public int Count()
        {
            return entities.Kategoris.Count();
        }


    }
}
