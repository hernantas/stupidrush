﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace StupidRush_Server
{
    class MatchContainer
    {
        public static Dictionary<int, List<Pool>> pools;
        public static List<Match> matchs;
        private static DispatcherTimer timer;
        private static MainWindow parent;

        private static void addMatch(int idSubkategori, Pool p1, Pool p2)
        {
            parent.AddStatus("Add Duel: " + p1.User.Nama + " vs " + p2.User.Nama);
            Match match = new Match();
            match.Player1 = p1;
            match.Player2 = p2;
            match.QuestionNumber = 0;
            match.IdSubKategori = idSubkategori;
            MatchContainer.matchs.Add(match);

            MatchContainer.pools[idSubkategori].Remove(p1);
            MatchContainer.pools[idSubkategori].Remove(p2);
        }

        private static void onInterval(object sender, EventArgs e)
        {
            int poolCount = 0;

            foreach (KeyValuePair<int, List<Pool>> pool in MatchContainer.pools)
            {
                poolCount += pool.Value.Count;
            }

            if (poolCount > 0) parent.AddStatus("Pools: " + poolCount);
            if (MatchContainer.matchs.Count > 0) parent.AddStatus("Match: " + MatchContainer.matchs.Count.ToString());

            foreach (KeyValuePair<int, List<Pool>> pool in MatchContainer.pools)
            {
                pool.Value.Sort(delegate(Pool p1, Pool p2)
                {
                    if (p1.Skors.Skor1 < p2.Skors.Skor1)
                        return -1;
                    else if (p1.Skors.Skor1 > p2.Skors.Skor1)
                        return 1;
                    return 0;
                });
            }

            foreach (KeyValuePair<int, List<Pool>> pool in MatchContainer.pools)
            {
                for (int i = 0; i < pool.Value.Count; i++)
                {
                    pool.Value[i].Range += 10;

                    if (i > 0 && Math.Abs(pool.Value[i].Skors.Skor1 - pool.Value[i - 1].Skors.Skor1) < pool.Value[i].Range)
                    {
                        addMatch(pool.Key, pool.Value[i], pool.Value[i - 1]);
                        i -= 2;
                    }

                    if (i < pool.Value.Count - 1 && (pool.Value[i].Skors.Skor1 - pool.Value[i + 1].Skors.Skor1) < pool.Value[i].Range)
                    {
                        addMatch(pool.Key, pool.Value[i], pool.Value[i + 1]);
                        i--;
                    }
                }
            }

            for (int i = 0; i < matchs.Count; )
            {
                if (matchs[i].isEnded && matchs[i].DelayEnd <= 0)
                {
                    matchs.RemoveAt(i);
                }
                else
                {
                    if (matchs[i].isEnded)
                    {
                        matchs[i].DelayEnd -= 1;
                    }
                    else
                    {
                        matchs[i].Timeout -= 1;
                        if (matchs[i].Timeout <= 0)
                        {
                            matchs[i].QuestionNumber = 7;
                            if (!matchs[i].P1Answer && !matchs[i].P2Answer) { matchs[i].Score1 = 0; matchs[i].Score2 = 0; }
                            else if (!matchs[i].P1Answer) matchs[i].Score1 = 0;
                            else if (!matchs[i].P2Answer) matchs[i].Score2 = 0;
                        }
                    }
                    i++;
                }
            }
        }

        public static void setParent(MainWindow window)
        {
            parent = window;
        }

        static MatchContainer()
        {
            pools = new Dictionary<int, List<Pool>>();
            matchs = new List<Match>();

            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 1000);
            timer.Tick += new EventHandler(onInterval);
            timer.Start();
        }
    }
}
