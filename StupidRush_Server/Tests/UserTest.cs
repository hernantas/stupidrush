﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StupidRush_Server;
using StupidRush_Server.Repository;
using StupidRush;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class UserTest
    {
        StupidRushEntities entities = new StupidRushEntities();
        [TestMethod]
        public void AddUserTest_Success()
        {
            //using (var entities = new StupidRushEntities())
            //{
            //    UserRepositoryDb reg = new UserRepositoryDb();
            //    int expectedValue = reg.Users.Count + 1;
            //    User u = new User()
            //    {
            //        Username = "aditya",
            //        Password = "123",
            //        Nama = "aditya",
            //        Lokasi = "Surabaya",
            //        TotalSkor = 0,
            //        Email = "lalayeye"
            //    };

            //    reg.AddUser(u);
            //    int resultValue = reg.Users.Count;
            //    Assert.AreEqual(expectedValue,resultValue);
            //}
        }

        [TestMethod]
        public void AddUserTest_Failed()
        {
            using (var entities = new StupidRushEntities())
            {
                UserRepositoryDb reg = new UserRepositoryDb();
                int expectedValue = reg.Users.Count;
                User u = new User()
                {
                    Username = "aditya",
                    Password = "123",
                    Nama = "aditya",
                    Lokasi = "Surabaya",
                    TotalSkor = 0,
                    Email = "lalayeye"
                };

                reg.AddUser(u);
                int resultValue = reg.Users.Count;
                Assert.AreEqual(expectedValue, resultValue);
            }
        }

        [TestMethod]
        public void GetUserTest_Success()
        {
            using (var entities = new StupidRushEntities())
            {
                UserRepositoryDb user = new UserRepositoryDb();
                var expectedResult = user.GetUser("aditya", "123");
                Assert.IsNotNull(expectedResult);
            }
        }

        public void GetUserTest_Failed()
        {
            using (var entities = new StupidRushEntities())
            {
                UserRepositoryDb user = new UserRepositoryDb();
                var expectedResult = user.GetUser("adi", "123");
                Assert.IsNotNull(expectedResult);
            }
        }

        [TestMethod]
        public void GetFindTest_Success()
        {
            using (var entities = new StupidRushEntities())
            {
                UserRepositoryDb user = new UserRepositoryDb();
                var resultValue = user.find("aditya", "123");
                bool expectedValue = true;
                Assert.AreEqual(expectedValue, resultValue);
            }
        }

        [TestMethod]
        public void GetFindTest_Failed()
        {
            using (var entities = new StupidRushEntities())
            {
                UserRepositoryDb user = new UserRepositoryDb();
                var resultValue = user.find("adin", "123");
                bool expectedValue = false;
                Assert.AreEqual(expectedValue, resultValue);
            }
        }

        [TestMethod]
        public void UpdateSkorTest_Success()
        {
            //using (var entities = new StupidRushEntities())
            //{
            //    List<int> result = new List<int>();
            //    UserRepositoryDb user = new UserRepositoryDb();
            //    int skorTambahan = 100;
            //    result = user.UpdateSkor("aditya","123", skorTambahan);
            //    int expectedValue = result[0] + skorTambahan;
            //    int resultValue = result[1];
            //    Assert.AreEqual(expectedValue, resultValue);
            //}
        }

        [TestMethod]
        public void UpdateSkorTest_Failed()
        {
            using (var entities = new StupidRushEntities())
            {
                List<int> result = new List<int>();
                UserRepositoryDb user = new UserRepositoryDb();
                int skorTambahan = 100;
                result = user.UpdateSkor("adin", "123", skorTambahan);
                int expectedValue = 0;
                int resultValue = result.Count;
                Assert.AreEqual(expectedValue, resultValue);
            }
        }


        [TestMethod]
        public void RankTest_Success()
        {
            using (var entities = new StupidRushEntities())
            {
                UserRepositoryDb user = new UserRepositoryDb();
                var U = user.Rank();
                Assert.IsNotNull(U);
            }
        }

        [TestMethod]
        public void LevelTest_Success()
        {
            using(var entities = new StupidRushEntities())
            {
                UserRepositoryDb user = new UserRepositoryDb();
                var U = user.Level("aditya");
                Assert.IsNotNull(U);
            }
        }
    }
}
