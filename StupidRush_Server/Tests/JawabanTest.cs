﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StupidRush_Server;
using StupidRush_Server.Repository;
using StupidRush;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    [TestClass]
    public class JawabanTest
    {
        private StupidRushEntities entities = new StupidRushEntities();

        [TestMethod]
        public void JawabanAddTest_Success()
        {
            //using (var entities = new StupidRushEntities())
            //{
            //    JawabanRepositoryDb krdb = new JawabanRepositoryDb();
            //    int expectedResult = krdb.Jawabans.Count + 1;
            //    int result = (int)entities.GetNewJawabanID().FirstOrDefault();
            //    Jawaban k = new Jawaban()
            //    {
            //        IDJawaban = result,
            //        IDSoal = 2,
            //        Jawaban1 = "lalala",
            //        Status = 1
            //    };
            //    krdb.AddJawaban(k);
            //    int ActualResult = krdb.Jawabans.Count;
            //    Assert.AreEqual(expectedResult, ActualResult);
            //}
        }
        [TestMethod]
        public void AddJawabanTest_Failed()
        {
            //using (var entities = new StupidRushEntities())
            //{
            //    JawabanRepositoryDb krdb = new JawabanRepositoryDb();
            //    int expectedResult = krdb.Jawabans.Count;
            //    int result = (int)entities.GetNewJawabanID().FirstOrDefault();
            //    Jawaban k = new Jawaban()
            //    {
            //        IDJawaban = 1,
            //        IDSoal = 2,
            //        Jawaban1 = "lalala",
            //        Status = 1
            //    };
            //    krdb.AddJawaban(k);
            //    int ActualResult = krdb.Jawabans.Count;
            //    Assert.AreEqual(expectedResult, ActualResult);
            //}
        }
        [TestMethod]
        public void DeleteJawabanTest_Success()
        {
            //using (var entities = new StupidRushEntities())
            //{
            //    JawabanRepositoryDb krdb = new JawabanRepositoryDb();
            //    int expectedResult = krdb.Jawabans.Count - 1;
            //    krdb.DeleteJawaban(8);

            //    int actualResult = krdb.Jawabans.Count;
            //    Assert.AreEqual(expectedResult, actualResult);
            //}

        }

        [TestMethod]
        public void DeleteJawabanTest_Failed()
        {
            using (var entities = new StupidRushEntities())
            {
                JawabanRepositoryDb krdb = new JawabanRepositoryDb();
                int expectedResult = krdb.Jawabans.Count;
                krdb.DeleteJawaban(1000);

                int actualResult = krdb.Jawabans.Count;
                Assert.AreEqual(expectedResult, actualResult);
            }

        }

    }
}
