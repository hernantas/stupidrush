//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace StupidRush_Server
{
    using System;
    using System.Collections.Generic;
    
    public partial class Soal
    {
        public Soal()
        {
            this.Jawabans = new HashSet<Jawaban>();
        }
    
        public int IDSoal { get; set; }
        public int IDSubKategori { get; set; }
        public string Soal1 { get; set; }
    
        public virtual ICollection<Jawaban> Jawabans { get; set; }
        public virtual SubKategori SubKategori { get; set; }
    }
}
