﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server
{
    class Match
    {
        public Pool Player1 { set; get; }
        public bool P1Answer { set; get; }
        public int Score1 = 0;

        public Pool Player2 { set; get; }
        public bool P2Answer { set; get; }
        public int Score2 = 0;

        public int IdSubKategori { set; get; }
        public List<Soal> listSoals;

        public double Timeout { set; get; }

        public int QuestionNumber { set; get; }
        public Soal Question { set; get; }
        public Jawaban Answer1 { set; get; }
        public Jawaban Answer2 { set; get; }
        public Jawaban Answer3 { set; get; }
        public Jawaban Answer4 { set; get; }

        public bool isEnded { set; get; }
        public int DelayEnd { set; get; }

        public int CorrectAnswer { set; get; }

        public Match()
        {
            Timeout = 20;
            isEnded = false;
            DelayEnd = 10;
            Score1 = 0;
            Score2 = 0;
            P1Answer = false;
            P2Answer = false;
            IdSubKategori = 0;
            QuestionNumber = 0;
            Question = null;
            Answer1 = null;
            Answer2 = null;
            Answer3 = null;
            Answer4 = null;
            CorrectAnswer = 0;
        }
    }
}
