﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public interface IUser
    {
        List<User> Users { get; }
        User GetUser(string userName, string pass);
        bool find(string username, string password);
        User Find(string username, string password);
        void AddUser(User u);
        List<int> UpdateSkor(string user, string pass, int skorSubCat);
        List<User> Rank();
        int Level(string username);

    }
}
