﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public interface ISoal
    {
        List<Soal> Soals { get; }

        void AddSoal(Soal b);

        void DeleteSoal(int id);
    }
}
