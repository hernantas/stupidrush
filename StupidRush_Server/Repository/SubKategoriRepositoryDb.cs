﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    class SubKategoriRepositoryDb : ISubKategori
    {
        public List<SubKategori> SubKategoris
        {
            get
            {
                List<SubKategori> result;

                using (StupidRushEntities context = new StupidRushEntities())
                {
                    result = context.SubKategoris.ToList<SubKategori>();
                }

                return result;
            }

        }

        public void AddSubKategori(SubKategori b)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                try
                {
                    context.SubKategoris.Add(b);
                    context.SaveChanges();
                }
                catch
                {

                }
            }
        }

        public void DeleteSubKategori(int id)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                SubKategori subKategoriToDelete = context.SubKategoris.Find(id);
                if (subKategoriToDelete != null)
                {
                    context.SubKategoris.Remove(subKategoriToDelete);
                    context.SaveChanges();
                }
            }
        }
    }
}
