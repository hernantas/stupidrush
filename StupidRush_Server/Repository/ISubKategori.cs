﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public interface ISubKategori
    {
        List<SubKategori> SubKategoris { get; }

        void AddSubKategori(SubKategori b);

        void DeleteSubKategori(int id);

    }
}
