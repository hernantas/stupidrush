﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public class UserRepositoryDb
    {
        public List<User> Users
        {
            get
            {
                List<User> result;

                using (StupidRushEntities context = new StupidRushEntities())
                {
                    result = context.Users.ToList<User>();
                }

                return result;
            }

        }

        public bool find(string username, string password)
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                 bool find = entities.Users.Any( c => c.Username.Equals(username) && c.Password.Equals(password));
                 return find;
            }
        }


        public User GetUser(string userName, string pass)
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                User results = (from c in entities.Users
                                where c.Username.Equals(userName) && c.Password.Equals(pass)
                                select c).FirstOrDefault<User>();
                return results;
            }

        }   
    
        public void AddUser(User u)
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                try
                {
                    entities.Users.Add(u);
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                }
            }
        }

        public List<int> UpdateSkor(string user, string pass, int skorSubCat)
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                List<int> result = new List<int>();
                try
                {
                    User userToUpdate = (from s in entities.Users
                                         where s.Username.Equals(user) && s.Password.Equals(pass)
                                         select s).FirstOrDefault();
                    if (userToUpdate != null)
                    {
                        result.Add((int)userToUpdate.TotalSkor);
                        userToUpdate.TotalSkor += skorSubCat;
                        entities.SaveChanges();
                        result.Add((int)userToUpdate.TotalSkor);
                    }
                    return result;

                }
                catch
                {
                    return result;
                }
            }
        }

        public List<User> Rank()
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                List<User> rank = (from p in entities.Users
                                   orderby p.TotalSkor descending
                                   select p).ToList();

                return rank;
            }
        }

        public int Level(string username)
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                int level = (int)(from p in entities.Users
                                  where p.Username.Equals(username)
                                  select p.TotalSkor).FirstOrDefault();
                return level / 100;
            }
        }



    }
}
