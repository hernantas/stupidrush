﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    class JawabanRepositoryDb : IJawaban
    {
        public List<Jawaban> Jawabans
        {
            get
            {
                List<Jawaban> result;

                using (StupidRushEntities context = new StupidRushEntities())
                {
                    result = context.Jawabans.ToList<Jawaban>();
                }

                return result;
            }

        }


        public void AddJawaban(Jawaban j)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                try
                {
                    context.Jawabans.Add(j);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
        }

        public void DeleteJawaban(int id)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                Jawaban jawabanToDelete = context.Jawabans.Find(id);
                if (jawabanToDelete != null)
                {
                    context.Jawabans.Remove(jawabanToDelete);
                    context.SaveChanges();
                }
            }
        }
    }
}
