﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    class SoalRepositoryDb : ISoal
    {
        public List<Soal> Soals
        {
            get
            {
                List<Soal> result;

                using (StupidRushEntities context = new StupidRushEntities())
                {
                    result = context.Soals.ToList<Soal>();
                }

                return result;
            }

        }


        public void AddSoal(Soal s)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                try
                {
                    context.Soals.Add(s);
                    context.SaveChanges();
                }
                catch
                {
                }
            }
        }

        public void DeleteSoal(int id)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                Soal soalToDelete = context.Soals.Find(id);
                if (soalToDelete != null)
                {
                    context.Soals.Remove(soalToDelete);
                    context.SaveChanges();
                }
            }
        }

    }
}
