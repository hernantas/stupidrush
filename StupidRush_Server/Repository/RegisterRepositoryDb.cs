﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public class RegisterRepositoryDb : IRegister
    {
        public List<User> Users
        {
            get
            {
                List<User> result;

                using (StupidRushEntities context = new StupidRushEntities())
                {
                    result = context.Users.ToList<User>();
                }

                return result;
            }

        }

        public void Register(User u)
        {
            using (StupidRushEntities entities = new StupidRushEntities())
            {
                try
                {
                    entities.Users.Add(u);
                    entities.SaveChanges();
                }
                catch (Exception ex)
                {
                }
            }
        }
    }
}
