﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public interface IRegister
    {
        List<User> Users { get; }

        void Register(User u);
    }
}
