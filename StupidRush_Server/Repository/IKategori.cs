﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public interface IKategori
    {
            List<Kategori> Kategoris { get; }

            void AddKategori(Kategori b);

            void DeleteKategori(int id);

            void Count();
        
    }
}
