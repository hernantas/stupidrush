﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Repository
{
    public interface IJawaban
    {
        List<Jawaban> Jawabans { get; }

        void AddJawaban(Jawaban b);

        void DeleteJawaban(int id);
        
    }
}
