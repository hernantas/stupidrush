﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace StupidRush_Server.Repository
{
    public class KategoriRepositoryDb : IKategori
    {

        public List<Kategori> Kategoris
        {
            get
            {
                List<Kategori> result;

                using (StupidRushEntities context = new StupidRushEntities())
                {
                    result = context.Kategoris.ToList<Kategori>();
                }

                return result;
            }

        }

        public void AddKategori(Kategori b)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                try
                {
                    context.Kategoris.Add(b);
                    context.SaveChanges();
                    //MessageBox.Show("Insert success!");
                }
                catch (Exception ex)
                {
                    //MessageBox.Show("Insert failed!");
                }
            }
        }

        public void DeleteKategori(int id)
        {
            using (StupidRushEntities context = new StupidRushEntities())
            {
                Kategori kategoriToDelete = context.Kategoris.Find(id);
                if (kategoriToDelete != null)
                {
                    context.Kategoris.Remove(kategoriToDelete);
                    context.SaveChanges();
                }
            }
        }

        public void Count()
        {
            throw new NotImplementedException();
        }
    }
}
