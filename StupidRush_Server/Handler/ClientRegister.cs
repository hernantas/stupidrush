﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StupidRush_Server.Controllers;

namespace StupidRush_Server.Handler
{
    public class ClientRegister : RequestHandler
    {
        public ClientRegister()
        {
            requestType = "register";
        }

        bool isUserFound()
        {
            
            string userName = Request["username_reg"];
            string passWord = Request["password_reg"];

            bool find = uc.find(userName,passWord);
            if (!find) return false;
            return true;
        }

        public void register()
        {
            try
            {
                if (isUserFound())
                {
                    Response.Add("valid", "0");
                    Response.Add("message", "Username " + Request["username_reg"] + " sudah ada!");
                }
                else
                {
                    string username = Request["username_reg"];
                    string password = Request["password_reg"];
                    string vpassword = Request["password_reg_repass"];
                    string location = Request["location"];
                    string email = Request["email"];
                    string nama = Request["nama"];
                    string cek = Request["cek"];
                   
                    if (username.Equals(""))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Kolom username harus diisi!");
                    }
                    if (password.Equals(""))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Kolom password harus diisi!");
                    }
                    if (!password.Equals(vpassword))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Password tidak sama!");
                    }
                    // || password.Equals("") || location.Equals("") || email.Equals("") || nama.Equals("")
                    if (nama.Equals(""))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Kolom nama harus diisi!");
                    }
                    if (email.Equals(""))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Kolom email harus diisi!");
                    }
                   
                    if (location.Equals(""))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Kolom lokasi harus diisi!");
                    }
                    if (cek.Equals("False"))
                    {
                        Response.Add("valid", "0");
                        Response.Add("message", "Anda harus bersedia menyetujui peraturan yang dibuat!");
                    }
                    if (!username.Equals("") && !password.Equals("") && !location.Equals("") && !email.Equals("") && !nama.Equals("") && !cek.Equals("False"))
                    {
                        User reg = new User()
                        {
                            Username = username,
                            Password = password,
                            Lokasi = location,
                            Email = email,
                            Nama = nama,
                            TotalSkor = 0

                        };
                        uc.Add(reg);

                        Response.Add("valid", "1");
                    }
                }

            }
            catch
            {
                
            }
        }

        public override void onReceiveRequest()
        {
            register();
        }
    }
}
