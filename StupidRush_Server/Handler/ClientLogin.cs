﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StupidRush_Server.Controllers;

namespace StupidRush_Server.Handler
{
    class ClientLogin : RequestHandler
    {
        private UserController uc = new UserController();
        public ClientLogin()
        {
            requestType = "login";
        }

        bool isUserFound()
        {
            string userName = Request["username"];
            string passWord = Request["password"];

            bool find = uc.find(userName,passWord);
            if (!find) return false;
            return true;
        }

        public override void onReceiveRequest()
        {
            if (!isUserFound())
            {
                Response.Add("valid", "0");
                Response.Add("message", "Username atau password salah!");
            }
            else
            {
                Response.Add("valid", "1");
            }
        }
    }
}
