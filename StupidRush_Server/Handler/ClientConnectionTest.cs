﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush_Server.Handler
{
    class ClientConnectionTest : RequestHandler
    {
        public ClientConnectionTest()
        {
            requestType = "connect";
        }

        public override void onReceiveRequest()
        {
            Response.Add("message", "fine");
        }
    }
}
