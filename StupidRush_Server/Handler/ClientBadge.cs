﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StupidRush_Server.Controllers;

namespace StupidRush_Server.Handler
{
    class ClientBadge : RequestHandler
    {
        private SkorController skrc = new SkorController();
        private UserController uc = new UserController();
        private SoalController sc = new SoalController();
        private JawabanController jc = new JawabanController();
        private KategoriController kc = new KategoriController();
        private SubKategoriController skc = new SubKategoriController();

        public ClientBadge()
        {
            requestType = "badge";
        }

        public override void onReceiveRequest()
        {
            string userName = Session["username"];

            int badge_1MATCH = skrc.FindUser(userName);
            int badge_barca = skrc.JumlahMenang(userName,9);
            int badge_komputer = skrc.JumlahMenang(userName, 1);
            int badge_kimia = skrc.JumlahMenang(userName, 2);
            int badge_film = skrc.JumlahMenang(userName, 8);
            int badge_mu = skrc.JumlahMenang(userName, 10);
            int badge_musik = skrc.JumlahMenang(userName, 7);
            int badge_internet = skrc.JumlahMenang(userName, 11);
            int badge_biologi = skrc.JumlahMenang(userName, 12);
            int badge_english = skrc.JumlahMenang(userName, 13);
            int badge_matematika = skrc.JumlahMenang(userName, 14);
            int badge_jumlahMenang1 = skrc.JumlahMenang(userName);
            int badge_jumlahMenang10 = skrc.JumlahMenang(userName);
            int badge_jumlahMenang50 = skrc.JumlahMenang(userName);
            int badge_jumlahMenang100 = skrc.JumlahMenang(userName);
            int badge_jumlahMenang250 = skrc.JumlahMenang(userName);
            int badge_level1 = uc.Level(userName);
            int badge_level5 = uc.Level(userName);
            int badge_level10 = uc.Level(userName);
            int badge_level25 = uc.Level(userName);

            if (badge_1MATCH >= 1) Response.Add("badge_1MATCH", "valid");
            if (badge_barca >= 10) Response.Add("badge_barca", "valid");
            if (badge_komputer >= 10) Response.Add("badge_komputer", "valid");
            if (badge_kimia >= 10) Response.Add("badge_kimia", "valid");
            if (badge_film >= 10) Response.Add("badge_film", "valid");
            if (badge_mu >= 10) Response.Add("badge_mu", "valid");
            if (badge_musik >= 10) Response.Add("badge_musik", "valid");
            if (badge_internet >= 10) Response.Add("badge_internet", "valid");
            if (badge_biologi >= 10) Response.Add("badge_biologi", "valid");
            if (badge_english >= 10) Response.Add("badge_english", "valid");
            if (badge_matematika >= 10) Response.Add("badge_matematika", "valid");
            if (badge_jumlahMenang1 >= 1) Response.Add("badge_WIN1", "valid");
            if (badge_jumlahMenang10 >= 10) Response.Add("badge_WIN10", "valid");
            if (badge_jumlahMenang50 >= 50) Response.Add("badge_WIN50", "valid");
            if (badge_jumlahMenang100 >= 100) Response.Add("badge_WIN100", "valid");
            if (badge_jumlahMenang250 >= 250) Response.Add("badge_WIN250", "valid");
            if (badge_level1 >= 1) Response.Add("badge_LV1", "valid");
            if (badge_level5 >= 5) Response.Add("badge_LV5", "valid");
            if (badge_level10 >= 10) Response.Add("badge_LV10", "valid");
            if (badge_level25 >= 25) Response.Add("badge_LV25", "valid");

            //The Champion
            if (badge_1MATCH >= 1 && badge_barca >= 1 && badge_komputer >= 1 && badge_kimia >= 1 && badge_film >= 1 && badge_film >= 1 && badge_mu >= 1 && badge_musik >= 1
               && badge_internet >= 1 && badge_biologi >= 1 && badge_english >= 1 && badge_matematika >= 1 && badge_jumlahMenang1 >= 1 && badge_jumlahMenang50 >= 50
               && badge_jumlahMenang100 >= 100 && badge_jumlahMenang250 >= 250 && badge_level1 >= 1 && badge_level5 >= 5 && badge_level10 >= 10 && badge_level25 >= 25)
                Response.Add("badge_champion", "valid");

        }
    }
}
