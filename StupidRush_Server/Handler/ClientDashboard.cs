﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StupidRush_Server.Controllers;

namespace StupidRush_Server.Handler
{
    class ClientDashboard : RequestHandler
    {

        public ClientDashboard()
        {
            requestType = "dashboard";
        }

        private void Filter()
        {
            string userName = Session["username"];
            string passWord = Session["password"];
            String kategori = Request["kategori"];
            int idKat = kc.getIDKategori(kategori);

            List<SubKategori> skat = skc.GetAllData(idKat);

            Response.Add("kategori", Request["kategori"]);
            for (int i = 1; i <= skat.Count(); i++)
            {    
                Response.Add("subkategori" + i, skat[i - 1].SubKategori1);
                 Response.Add("subkategoriexp" + i, skrc.skorPerSubKategori(userName,skat[i-1].IDSubKategori).ToString());
            }

        }

        public override void onReceiveRequest()
        {
            if (!checkAuth()) return;

            if (Request.Count > 0)
            {
                Filter();
                return;
            }

            string userName = Session["username"];
            string passWord = Session["password"];
            User find = uc.Find(userName, passWord);

            List<Kategori> kat = kc.GetCategory();

            List<User> rank = uc.Rank();

            Response.Add("nama", find.Nama.ToString());
            Response.Add("lokasi", find.Lokasi.ToString());
            Response.Add("exp", find.TotalSkor.ToString());
            
            for (int i = 1; i <= kat.Count(); i++)
            {
                Response.Add("kategori"+i, kat[i-1].Kategori1);
            }

            
            for (int i = 1; i <= 5; i++)
            {
                int id = 0;
                for (int j= 0; j < rank[i - 1].Username.Length; j++)
                {
                    id += Convert.ToInt16(rank[i - 1].Username[j]);
                }
                id %= 10;
                Response.Add("rankname" + i, rank[i - 1].Nama);
                Response.Add("rankgambar" + i, id.ToString());
                Response.Add("rankskor" + i, rank[i - 1].TotalSkor.ToString());
            }
            int id2 = 0;
            for (int i = 0; i < userName.Length; i++)
            {
                   id2 += Convert.ToInt16(userName[i]);
            }
            id2 = id2 % 10;
            Response.Add("idgambar", id2.ToString());
            Response.Add("jumlahmenang",skrc.JumlahMenang(userName).ToString());
            
        }
    }
}
