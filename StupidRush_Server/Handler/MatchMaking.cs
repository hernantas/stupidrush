﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using StupidRush_Server.Controllers;
namespace StupidRush_Server.Handler
{
    class MatchMaking : RequestHandler
    {

        public MatchMaking()
        {
            requestType = "match";
        }

        private void addMatch(int idSubkategori, Pool p1, Pool p2)
        {
            parent.AddStatus("Add Duel: "+ p1.User.Nama + " vs " + p2.User.Nama);
            Match match = new Match();
            match.Player1 = p1;
            match.Player2 = p2;
            match.QuestionNumber = 0;
            match.IdSubKategori = idSubkategori;
            MatchContainer.matchs.Add(match);

            MatchContainer.pools[idSubkategori].Remove(p1);
            MatchContainer.pools[idSubkategori].Remove(p2);
        }

        private void addToPool()
        {
            bool userInPool = false;
            String username = Session["username"];
            String password = Session["password"];
            String subKat = Request["subkategori"];
            SubKategori subKategori = skc.getSubKategori(subKat);
            User user = uc.GetUser(username, password);
            Skor skor = skrc.GetSkor(username, subKategori.IDSubKategori);

            if (MatchContainer.pools.ContainsKey(subKategori.IDSubKategori))
            {
                foreach (Pool p in MatchContainer.pools[subKategori.IDSubKategori])
                {
                    if (p.User.Username.Equals(Session["username"]) && p.User.Password.Equals(Session["password"]))
                    {
                        userInPool = true;
                        Response["act"] = "wait";
                        break;
                    }
                }
            }

            if (!userInPool)
            {
                if (!MatchContainer.pools.ContainsKey(subKategori.IDSubKategori))
                {
                    MatchContainer.pools[subKategori.IDSubKategori] = new List<Pool>();
                }

                if (skor == null)
                {
                    skor = new Skor();
                    skor.Skor1 = 0;
                }

                Pool pool = new Pool();
                pool.User = user;
                pool.Skors = skor;
                pool.Range = 0;

                MatchContainer.pools[subKategori.IDSubKategori].Add(pool);

                parent.AddStatus("Add user "+user.Nama+" to Pool");

                Response["act"] = "wait";
            }
        }

        private void findMatch()
        {
            String username = Session["username"];
            String password = Session["password"];
            bool matchFound = false;

            foreach (Match match in MatchContainer.matchs)
            {
                if ((match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
                    || (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password)))
                {
                    if (match.isEnded)
                    {
                        Response["act"] = "wait";
                        return;
                    }

                    matchFound = true;
                    break;
                }
            }

            if (matchFound)
            {
                Response["act"] = "duel";
            }
            else
            {
                Response["act"] = "wait";
            }
            
        }

        private void doScore(Match match)
        {
            String username = Session["username"];
            String password = Session["password"];

            int id = 0;
            for (int i = 0; i < match.Player1.User.Username.Length; i++)
            {
                id += Convert.ToInt16(match.Player1.User.Username[i]);
            }
            id = id % 10;

            int id2 = 0;
            for (int i = 0; i < match.Player2.User.Username.Length; i++)
            {
                id2 += Convert.ToInt16(match.Player2.User.Username[i]);
            }
            id2 = id2 % 10;

            if (match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
            {
                Response.Add("playerid", id.ToString());
                Response.Add("opponentid", id2.ToString());
            }
            else if (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password))
            {
                Response.Add("opponentid", id.ToString());
                Response.Add("playerid", id2.ToString());
            }

            Response["act"] = "score";
        }

        private void generateQuestion(Match match)
        {
            if (match.QuestionNumber == 6)
            {
                match.QuestionNumber = 7;
                return;
            }

            Random rnd = new Random();

            int randomSoal = rnd.Next(0, match.listSoals.Count);
            match.Question = match.listSoals[randomSoal];
            match.listSoals.RemoveAt(randomSoal);

            List<Jawaban> jawabanCorrect = jc.GetJawabanCorrect(match.Question.IDSoal);

            List<Jawaban> jawabWrong = jc.GetJawabanWrong(match.Question.IDSoal);

            List<Jawaban> jawabanWrong = new List<Jawaban>();

            foreach (Jawaban jaw in jawabWrong)
            {
                jawabanWrong.Add(jaw);
            }

            bool jawabanNotFour = false;
            int randomJawaban = 0;

            randomJawaban = rnd.Next(0, jawabanWrong.Count);
            match.Answer1 = jawabanWrong[randomJawaban];
            jawabanWrong.RemoveAt(randomJawaban);

            randomJawaban = rnd.Next(0, jawabanWrong.Count);
            match.Answer2 = jawabanWrong[randomJawaban];
            jawabanWrong.RemoveAt(randomJawaban);

            randomJawaban = rnd.Next(0, jawabanWrong.Count);
            match.Answer3 = jawabanWrong[randomJawaban];
            jawabanWrong.RemoveAt(randomJawaban);

            if (jawabanWrong.Count > 0)
            {
                randomJawaban = rnd.Next(0, jawabanWrong.Count);
                match.Answer4 = jawabanWrong[randomJawaban];
                jawabanWrong.RemoveAt(randomJawaban);
            }
            else
            {
                jawabanNotFour = true;
            }

            randomJawaban = rnd.Next(0, jawabanCorrect.Count);

            int randomCorrect = rnd.Next(1, 4);

            switch (randomCorrect)
            {
                case 1:
                    if (jawabanNotFour) match.Answer4 = match.Answer1;
                    match.Answer1 = jawabanCorrect[randomJawaban];
                    break;
                case 2: 
                    if (jawabanNotFour) match.Answer4 = match.Answer2;
                    match.Answer2 = jawabanCorrect[randomJawaban]; 
                    break;
                case 3: 
                    if (jawabanNotFour) match.Answer4 = match.Answer3;
                    match.Answer3 = jawabanCorrect[randomJawaban]; 
                    break;
                case 4: match.Answer4 = jawabanCorrect[randomJawaban]; break;
            }

            match.CorrectAnswer = randomCorrect;

            match.P1Answer = false;
            match.P2Answer = false;
            match.Timeout = 20;

            match.QuestionNumber++;
        }

        private void getUser()
        {
            String username = Session["username"];
            String password = Session["password"];

            User user1 = null;
            User user2 = null;

            foreach (Match match in MatchContainer.matchs)
            {
                int id = 0;
                for (int i = 0; i < match.Player1.User.Username.Length; i++)
                {
                    id += Convert.ToInt16(match.Player1.User.Username[i]);
                }
                id = id % 10;
                
                int id2 = 0;
                for (int i = 0; i < match.Player2.User.Username.Length; i++)
                {
                    id2 += Convert.ToInt16(match.Player2.User.Username[i]);
                }
                id2 = id2 % 10;

                if (match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
                {
                    Response.Add("player1ID", id.ToString());
                    Response.Add("player2ID", id2.ToString());
                    user1 = match.Player1.User;
                    user2 = match.Player2.User;
                    break;
                }
                if (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password))
                {
                    Response.Add("player2ID", id.ToString());
                    Response.Add("player1ID", id2.ToString());
                    user1 = match.Player2.User;
                    user2 = match.Player1.User;
                    break;
                }
            }

            Response["nama1"] = user1.Nama;
            Response["nama2"] = user2.Nama;
        }

        private void getQuestion()
        {
            String username = Session["username"];
            String password = Session["password"];
            

            foreach (Match match in MatchContainer.matchs)
            {
                if ((match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
                    || (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password)))
                {
                    if (match.QuestionNumber >= 7)
                    {
                        doScore(match);
                      
                        if (!match.isEnded)
                        {
                            match.isEnded = true;

                            Skor skor1 = skrc.GetSkor(match.Player1.User.Username,match.IdSubKategori);
                            Skor skor2 = skrc.GetSkor(match.Player2.User.Username, match.IdSubKategori);
                            
                            if (skor1 == null)
                            {
                                int newSkorID = (int)(entities.GetNewSkorID().FirstOrDefault());
                              
                                Skor newSkor1 = new Skor(){
                                    IDSkor = newSkorID,
                                    IDSubKategori = match.IdSubKategori,
                                    Skor1 = 0,
                                    Username = match.Player1.User.Username                                    
                                };
                                skrc.Add(newSkor1);
                                skor1 = skrc.GetSkor(match.Player1.User.Username, match.IdSubKategori);
                            }

                            if (skor2 == null)
                            {
                                int newSkorID = (int)entities.GetNewSkorID().FirstOrDefault();
                                Skor newSkor1 = new Skor()
                                {
                                    IDSkor = newSkorID,
                                    IDSubKategori = match.IdSubKategori,
                                    Skor1 = 0,
                                    Username = match.Player2.User.Username
                                };
                                skrc.Add(newSkor1);
                                skor2 = skrc.GetSkor(match.Player2.User.Username, match.IdSubKategori);
                            }

                            skrc.Update(skor1, match.Score1);
                            skrc.Update(skor2, match.Score1);

                            uc.UpdateSkor(match.Player1.User.Username, match.Player1.User.Password, match.Score1);
                            uc.UpdateSkor(match.Player2.User.Username, match.Player2.User.Password, match.Score2);

                            if (match.Score1 > match.Score2)
                            {
                                skrc.tambahMenang(skor1);
                            }
                            else if (match.Score1 < match.Score2)
                            {
                                skrc.tambahMenang(skor2);
                            }
                        }
                    }

                    if (!match.isEnded)
                    {
                        if (match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
                        {
                            Response["skor1"] = match.Score1.ToString();
                            Response["skor2"] = match.Score2.ToString();
                        }

                        if (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password))
                        {
                            Response["skor1"] = match.Score2.ToString();
                            Response["skor2"] = match.Score1.ToString();
                        }
                    }

                    if (match.QuestionNumber == 0)
                    {
                        int idSubKategori = match.IdSubKategori;
                        List<Soal> soal = sc.GetAllData(idSubKategori);

                        match.listSoals = soal;

                        generateQuestion(match);

                        Response["soal"] = match.Question.Soal1;
                        Response["jawaban1"] = match.Answer1.Jawaban1;
                        Response["jawaban2"] = match.Answer2.Jawaban1;
                        Response["jawaban3"] = match.Answer3.Jawaban1;
                        Response["jawaban4"] = match.Answer4.Jawaban1;
                    }
                    else
                    {
                        if (match.P1Answer && match.P2Answer && !match.isEnded)
                        {
                            generateQuestion(match);
                        }

                        Response["soal"] = match.Question.Soal1;
                        Response["jawaban1"] = match.Answer1.Jawaban1;
                        Response["jawaban2"] = match.Answer2.Jawaban1;
                        Response["jawaban3"] = match.Answer3.Jawaban1;
                        Response["jawaban4"] = match.Answer4.Jawaban1;
                    }
                }
            }
        }

        private void giveAnswer()
        {
            String username = Session["username"];
            String password = Session["password"];

            foreach (Match match in MatchContainer.matchs)
            {
                if ((match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
                    || (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password)))
                {
                    Response["answer"] = Request["answer"];
                    double time = double.Parse(Request["time"]);

                    bool correct = false;

                    if (match.CorrectAnswer == int.Parse(Request["answer"]))
                    {
                        Response["crt"] = "1";
                        correct = true;
                    }
                    else
                    {
                        Response["crt"] = "0";
                        correct = false;
                    }

                    if (time <= 0) time = 0;

                    if (match.Player1.User.Username.Equals(username) && match.Player1.User.Password.Equals(password))
                    {
                        if (!match.P1Answer && correct) match.Score1 += (int)(time * 2);
                        match.P1Answer = true;
                    }
                    if (match.Player2.User.Username.Equals(username) && match.Player2.User.Password.Equals(password))
                    {
                        if (!match.P2Answer && correct) match.Score2 += (int)(time * 2);
                        match.P2Answer = true;
                    }
                }
            }
        }

        public override void onReceiveRequest()
        {
            switch (Request["type"])
            {
                case "pool": addToPool(); break;
                case "find": findMatch(); break;
                case "getuser": getUser(); break;
                case "getquestion": getQuestion(); break;
                case "giveanswer": giveAnswer(); break;
            }
        }
    }
}
