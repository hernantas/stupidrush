﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using StupidRush_Server.Handler;
using System.IO;

namespace StupidRush_Server
{
    class ServerHandler
    {
        private bool isClientProcessing = false;
        private MainWindow parent;
        private TcpListener serverSocket;
        private List<Thread> threadList;
        private int port = 1993;
        public bool Listening { set; get; }

        public ServerHandler(MainWindow window)
        {
            Listening = true;
            parent = window;
            threadList = new List<Thread>();
        }

        public void Handler(List<RequestHandler> requestHandler)
        {
            // Add Handler here
            AddHandler(requestHandler, new ClientConnectionTest());
            AddHandler(requestHandler, new ClientLogin());
            AddHandler(requestHandler, new ClientDashboard());
            AddHandler(requestHandler, new ClientRegister());
            AddHandler(requestHandler, new MatchMaking());
            AddHandler(requestHandler, new ClientBadge());
        }

        ~ServerHandler()
        {
            foreach (Thread thread in threadList)
            {
                thread.Abort();
            }
            threadList.Clear();

            serverSocket.Stop();
        }

        public void Start()
        {
            serverSocket = new TcpListener(LocalIPAddress(), port);
            serverSocket.Start();

            parent.AddStatus("Server Started");

            parent.AddStatus("Start Listening at "+LocalIPAddress()+", port: "+port);

            while (Listening)
            {
                if (serverSocket.Pending() && !isClientProcessing)
                {
                    // Start new thread if there is pending connection and last connection is not processing
                    Thread thread = new Thread(new ThreadStart(Run));
                    isClientProcessing = true;
                    thread.Start();
                    threadList.Add(thread);
                }

                for (int i = 0; i < threadList.Count; )
                {
                    if (threadList[i].IsAlive)
                    {
                        i++;
                    }
                    else
                    {
                        threadList.RemoveAt(i);
                    }
                }

                Thread.Sleep(1);
            }
        }

        public void Run()
        {
            TcpClient clientSocket = default(TcpClient);
            clientSocket = serverSocket.AcceptTcpClient();
            parent.AddStatus("Accept new connection");
            isClientProcessing = false;
            List<RequestHandler> requestHandler = new List<RequestHandler>();

            Handler(requestHandler);

            while (true)
            {
                try
                {
                    NetworkStream stream = clientSocket.GetStream();

                    int bufferSize = (int)clientSocket.ReceiveBufferSize;
                    byte[] buffer = new byte[bufferSize];
                    stream.Read(buffer, 0, bufferSize);
                    String message = System.Text.Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                    message = message.Replace("\0", String.Empty);
                    parent.AddStatus("Receive request: " + message);

                    if (message.Equals("disconnect"))
                    {
                        parent.AddStatus("User disconnected");
                        break;
                    }
                    else
                    {
                        String response = onReceiveMessage(requestHandler, message);
                        buffer = new byte[response.Length];
                        buffer = System.Text.Encoding.UTF8.GetBytes(response);
                        parent.AddStatus("Sent response: " + response);
                        stream.Write(buffer, 0, buffer.Length);
                        stream.Flush();
                    }
                }
                catch (IOException ex)
                {
                    parent.AddStatus("User force disconnect");
                    break;
                }
            }

            clientSocket.Close();
        }

        public void AddHandler(List<RequestHandler> requestHandler, RequestHandler handler)
        {
            handler.setParent(parent);
            requestHandler.Add(handler);
        }

        private String onReceiveMessage(List<RequestHandler> requestHandler, String message)
        {
            String responseMsg = "";

            Dictionary<String, String> request = new Dictionary<string,string>();
            string[] messagePackage = message.Split('?');

            if (messagePackage.Length != 3) return "Invalid Request";

            string requestType = messagePackage[0];
            string[] extractedMessage = messagePackage[1].Split('&');
            string[] extractedSession = messagePackage[2].Split('&');

            foreach (string inMsg in extractedMessage)
            {
                parent.AddStatus("|-- Extract Request: " + inMsg);
            }

            foreach (RequestHandler handler in requestHandler)
            {
                handler.Clear();

                if (handler.RequestType == requestType)
                {

                    foreach (string inMsg in extractedMessage)
                    {
                        string[] variable = inMsg.Split('=');
                        
                        if (variable.Length == 2)
                            handler.Request.Add(variable[0], variable[1]);
                    }

                    foreach (string inMsg in extractedSession)
                    {
                        string[] variable = inMsg.Split('=');

                        if (variable.Length == 2)
                        {
                            parent.AddStatus("|--Seesion "+variable[0]+"="+variable[1]);
                            handler.Session.Add(variable[0], variable[1]);
                        }
                    }

                    handler.onReceiveRequest();

                    foreach (KeyValuePair<String, String> entry in handler.Response)
                    {
                        if (responseMsg.Equals(""))
                        {
                            responseMsg = entry.Key + "=" + entry.Value;
                        }
                        else
                        {
                            responseMsg += "&" + entry.Key + "=" + entry.Value;
                        }
                    }
                }
            }

            responseMsg = "key="+((new Random()).Next(0, 999999999)).ToString()+"&" + responseMsg;

            return responseMsg;
        }

        /// <summary>
        /// Utility function to get current ip address
        /// </summary>
        /// <returns>Return this machine ip address on local network</returns>
        public static IPAddress LocalIPAddress()
        {
            IPHostEntry host;
            IPAddress localIP = null;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip;
                }
            }
            return localIP;
        }
    }
}
