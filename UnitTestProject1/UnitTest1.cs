﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StupidRush_Server;
using StupidRush_Server.Repository;
using StupidRush;
namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
            
        [TestMethod]
        public void TestMethod1()
        {
            KategoriRepositoryDb krdb = new KategoriRepositoryDb();

            int expectedresult = 6;

            Kategori k = new Kategori(100, "aditya");
            krdb.AddKategori(k);

            int result = krdb.Kategoris.Count;

            Assert.AreEqual(expectedresult, result);

        }
    }
}
