﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace StupidRush
{
    class RequestManager
    {
        private TcpClient clientSocket;
        private Thread threadRequest;
        private List<RequestMessage> RequestQueue;
        private Dictionary<String, String> session;

        int port = 1993;

        public RequestManager()
        {
            RequestQueue = new List<RequestMessage>();
            session = new Dictionary<string, string>();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0,0, 0, 0, 250);
            timer.Tick += new EventHandler(RequestChecker);
            timer.Start();
        }
        ~RequestManager()
        {
            if (threadRequest != null && threadRequest.ThreadState == ThreadState.Running) threadRequest.Abort();
            threadRequest = null;
        }

        private void Connect()
        {
            clientSocket = new TcpClient();
            int tryCount = 3;
            while (tryCount-- > 0)
            {
                try
                {
                    clientSocket.Connect("192.168.1.2", port);
                    break;
                }
                catch (Exception ex)
                {
                    
                }

                Thread.Sleep(1);
            }
        }
        private void Disconnect()
        {
            clientSocket.Close();
        }

        private void RequestSender()
        {
            if (RequestQueue.Count > 0) Connect();

            for (int i = 0; i < RequestQueue.Count; i++ )
            {
                RequestMessage req = RequestQueue[i];

                if (req.IsResponded == false)
                {
                    string requestString = "";

                    foreach (KeyValuePair<String, String> entry in req.Request.getRequest())
                    {
                        if (requestString.Equals(""))
                        {
                            requestString = entry.Key + "=" + entry.Value;
                        }
                        else
                        {
                            requestString += "&" + entry.Key + "=" + entry.Value;
                        }
                    }

                    string sessionStr = "";
                    foreach (KeyValuePair<String, String> entry in session)
                    {
                        if (sessionStr.Equals(""))
                        {
                            sessionStr = entry.Key + "=" + entry.Value;
                        }
                        else
                        {
                            sessionStr += "&" + entry.Key + "=" + entry.Value;
                        }
                    }

                    requestString = req.Request.getType() + "?" + requestString + "?" + sessionStr;

                    NetworkStream stream = clientSocket.GetStream();
                    byte[] buffer = System.Text.Encoding.UTF8.GetBytes(requestString);
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Flush();

                    buffer = new byte[clientSocket.ReceiveBufferSize];
                    stream.Read(buffer, 0, clientSocket.ReceiveBufferSize);
                    String serverResponse = System.Text.Encoding.UTF8.GetString(buffer);
                    serverResponse = serverResponse.Replace("\0", String.Empty);
                    req.Response = serverResponse;
                    req.IsResponded = true;
                }
            }

            if (isConnected()) Disconnect();
        }

        public Dictionary<String, String> getSession()
        {
            return session;
        }

        private void RequestChecker(object sender, EventArgs e)
        {
            if (threadRequest == null || threadRequest.ThreadState != ThreadState.Running)
            {
                for (int i = RequestQueue.Count - 1; i >= 0; i--)
                {
                    if (RequestQueue[i].IsResponded && RequestQueue[i].Handler != null)
                    {
                        string[] responses = RequestQueue[i].Response.Split('&');

                        Dictionary<String, String> dResponse = new Dictionary<string,string>();

                        foreach (string response in responses)
                        {
                            string[] variable = response.Split('=');

                            if (variable.Length == 2)
                                dResponse.Add(variable[0], variable[1]);
                        }

                        RequestQueue[i].Handler(dResponse);
                        RequestQueue.RemoveAt(i);
                    }
                }

                threadRequest = new Thread(new ThreadStart(RequestSender));
                threadRequest.Start();
            }
        }

        public void SendRequest(Request message, RequestMessage.ResponseHandler handler)
        {
            RequestQueue.Add(new RequestMessage(message, handler));
        }

        public void SendRequest(Request message)
        {
            RequestQueue.Add(new RequestMessage(message, null));
        }

        public bool isConnected()
        {
            if (clientSocket == null) return false;
            return clientSocket.Connected;
        }
    }
}
