﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush
{
    class Request
    {
        private String type;
        private Dictionary<String, String> requestMessages;

        public Request(String type)
        {
            this.type = type;
            requestMessages = new Dictionary<string, string>();
        }

        public void Add(String key, String value)
        {
            requestMessages.Add(key, value);
        }

        public String getType()
        {
            return type;
        }
        public Dictionary<String, String> getRequest()
        {
            return requestMessages;
        }
    }
}
