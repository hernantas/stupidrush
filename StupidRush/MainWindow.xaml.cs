﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace StupidRush
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private bool isAnswered = false;
        private double duelTimer = -1;
        private double loadingDuelTimer = -1;
        private double barDelay = 0;
        private List<String> errorMsg;
        private List<String> notificationMsg;
        private RequestManager Server;
        private DispatcherTimer barTimer;

        public MainWindow()
        {
            InitializeComponent();

#if DEBUG
            Duel duel = new Duel();
            duel.Show();
#endif
        }
        public MainWindow(bool noduel)
        {
            InitializeComponent();
        }

        private void AddErrorMessage(String message)
        {
            errorMsg.Add(message);
        }
        private void AddNotificationMessage(String message)
        {
            notificationMsg.Add(message);
        }

        private void MessageDisplayer(object sender, EventArgs e)
        {
            if (barDelay == 0)
            {
                if (notificationMsg.Count == 0)
                {
                    bar_notification.Visibility = Visibility.Hidden;
                }
                if (errorMsg.Count == 0)
                {
                    bar_errormessage.Visibility = Visibility.Hidden;
                }

                if (errorMsg.Count > 0)
                {
                    bar_errormessage.Content = errorMsg[0];
                    bar_errormessage.Visibility = Visibility.Visible;
                    errorMsg.RemoveAt(0);
                    barDelay = 3;
                }
                else if (notificationMsg.Count > 0)
                {
                    bar_notification.Content = notificationMsg[0];
                    bar_notification.Visibility = Visibility.Visible;
                    notificationMsg.RemoveAt(0);

                    barDelay = 3;
                }
            }
            else
            {
                barDelay-= 0.25;
            }

            if (loadingDuelTimer >= 0)
            {
                if (loadingDuelTimer % 1 == 0)
                {
                    if (label_findOpponent.Content.Equals("Finding Opponent")) label_findOpponent.Content = "Finding Opponent.";
                    else if (label_findOpponent.Content.Equals("Finding Opponent.")) label_findOpponent.Content = "Finding Opponent..";
                    else if (label_findOpponent.Content.Equals("Finding Opponent..")) label_findOpponent.Content = "Finding Opponent...";
                    else if (label_findOpponent.Content.Equals("Finding Opponent...")) label_findOpponent.Content = "Finding Opponent";
                }

                int delay = 1;
                if (loadingDuelTimer == delay * 1) canvas_green.Visibility = Visibility.Visible;
                if (loadingDuelTimer == delay * 2) canvas_yellow.Visibility = Visibility.Visible;
                if (loadingDuelTimer == delay * 3) canvas_blue.Visibility = Visibility.Visible;
                if (loadingDuelTimer == delay * 4) canvas_red.Visibility = Visibility.Visible;
                if (loadingDuelTimer == delay * 5) canvas_gb.Visibility = Visibility.Visible;

                if (loadingDuelTimer == delay * 10) canvas_green.Visibility = Visibility.Hidden;
                if (loadingDuelTimer == delay * 9) canvas_yellow.Visibility = Visibility.Hidden;
                if (loadingDuelTimer == delay * 8) canvas_blue.Visibility = Visibility.Hidden;
                if (loadingDuelTimer == delay * 7) canvas_red.Visibility = Visibility.Hidden;
                if (loadingDuelTimer == delay * 6) canvas_gb.Visibility = Visibility.Hidden;

                if (loadingDuelTimer >= delay * 10) loadingDuelTimer = 0;

                loadingDuelTimer += 0.25;
            }

            if (duelTimer == 0 && !isAnswered)
            {
                isAnswered = true;
                Request request = new Request("match");
                request.Add("type", "giveanswer");
                request.Add("time", duelTimer.ToString());
                request.Add("answer", "-1");
                Server.SendRequest(request, MatchAnwer);
                duelTimer = -1;
            }
            else if (duelTimer > 0.0f && !isAnswered)
            {
                label_countdown.Content = ((int)duelTimer).ToString();
                duelTimer -= 0.25;
            }
        }

        private void DuelButtonClear()
        {
            txt_questiondisplay.Foreground = Brushes.Black;
            button_answeroption1.Background = Brushes.Gray;
            button_answeroption2.Background = Brushes.Gray;
            button_answeroption3.Background = Brushes.Gray;
            button_answeroption4.Background = Brushes.Gray;
        }

        private void DuelButtonCorrect(int index)
        {
            DuelButtonClear();
            switch(index)
            {
                case 0: txt_questiondisplay.Foreground = Brushes.LightGreen; break;
                case 1: button_answeroption1.Background = Brushes.LightGreen; break;
                case 2: button_answeroption2.Background = Brushes.LightGreen; break;
                case 3: button_answeroption3.Background = Brushes.LightGreen; break;
                case 4: button_answeroption4.Background = Brushes.LightGreen; break;
            }
        }

        private void DuelButtonWrong(int index)
        {
            DuelButtonClear();
            switch (index)
            {
                case 0: txt_questiondisplay.Foreground = Brushes.LightPink; break;
                case 1: button_answeroption1.Background = Brushes.LightPink; break;
                case 2: button_answeroption2.Background = Brushes.LightPink; break;
                case 3: button_answeroption3.Background = Brushes.LightPink; break;
                case 4: button_answeroption4.Background = Brushes.LightPink; break;
            }
        }

        private void MatchAnwer(Dictionary<string, string> response)
        {
            if (response["crt"].Equals("1"))
            {
                DuelButtonCorrect(int.Parse(response["answer"]));
            }
            else if (response["crt"].Equals("0"))
            {
                DuelButtonWrong(int.Parse(response["answer"]));
            }

            Request request = new Request("match");
            request.Add("type", "getquestion");
            Server.SendRequest(request, MatchDuel);
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            Server = new RequestManager();
            errorMsg = new List<string>();
            notificationMsg = new List<string>();

            barTimer = new DispatcherTimer();
            barTimer.Interval = new TimeSpan(0, 0, 0, 0, 250);
            barTimer.Tick += new EventHandler(MessageDisplayer);
            barTimer.Start();
        }

        public void ConnectionCheck(Dictionary<String, String> response)
        {
            AddNotificationMessage("Connect");
        }

        public void LoginCheck(Dictionary<String, String> response)
        {

            if (response["valid"].Equals("0"))
            {
                AddErrorMessage(response["message"]);
            }
            else
            {
                Server.getSession().Add("username", txtLogin_Username.Text);
                Server.getSession().Add("password", txtLogin_Password.Password);

                Login.Visibility = Visibility.Hidden;
                Dashboard.Visibility = Visibility.Visible;

                DashboardDisplay();
            }

            txtLogin_Username.Text = "";
            txtLogin_Password.Password = "";
        }

        public void DashboardDisplay()
        {
            Server.SendRequest(new Request("dashboard"), DashboardHandler);
            Server.SendRequest(new Request("badge"), BadgeHandler);
        }

        private void BadgeHandler(Dictionary<string, string> response)
        {
            pic_1MATCH.Opacity = 0.20;
            pic_BARCA.Opacity = 0.20;
            pic_KOMPUTER.Opacity = 0.20;
            pic_KIMIA.Opacity = 0.20;
            pic_FILM.Opacity = 0.20;
            pic_MU.Opacity = 0.20;
            pic_MUSIK.Opacity = 0.20;
            pic_INTERNET.Opacity = 0.20;
            pic_BIOLOGI.Opacity = 0.20;
            pic_ENGLISH.Opacity = 0.20;
            pic_MATEMATIKA.Opacity = 0.20;
            pic_10WIN.Opacity = 0.20;
            pic_50WIN.Opacity = 0.20;
            pic_100WIN.Opacity = 0.20;
            pic_250WIN.Opacity = 0.20;
            pic_10WIN.Opacity = 0.20;
            pic_50WIN.Opacity = 0.20;
            pic_100WIN.Opacity = 0.20;
            pic_250WIN.Opacity = 0.20;
            pic_CHAMP.Opacity = 0.20;

            if (response.ContainsKey("badge_1MATCH")) pic_1MATCH.Opacity = 1;
            if (response.ContainsKey("badge_barca")) pic_BARCA.Opacity = 1;
            if (response.ContainsKey("badge_komputer")) pic_KOMPUTER.Opacity = 1;
            if (response.ContainsKey("badge_kimia")) pic_KIMIA.Opacity = 1;
            if (response.ContainsKey("badge_film")) pic_FILM.Opacity = 1;
            if (response.ContainsKey("badge_mu")) pic_MU.Opacity = 1;
            if (response.ContainsKey("badge_musik")) pic_MUSIK.Opacity = 1;
            if (response.ContainsKey("badge_internet")) pic_INTERNET.Opacity = 1;
            if (response.ContainsKey("badge_biologi")) pic_BIOLOGI.Opacity = 1;
            if (response.ContainsKey("badge_english")) pic_ENGLISH.Opacity = 1;
            if (response.ContainsKey("badge_matematika")) pic_MATEMATIKA.Opacity = 1;
            if (response.ContainsKey("badge_WIN10")) pic_10WIN.Opacity = 1;
            if (response.ContainsKey("badge_WIN50")) pic_50WIN.Opacity = 1;
            if (response.ContainsKey("badge_WIN100")) pic_100WIN.Opacity = 1;
            if (response.ContainsKey("badge_WIN250")) pic_250WIN.Opacity = 1;
            if (response.ContainsKey("badge_LV1")) pic_10WIN.Opacity = 1;
            if (response.ContainsKey("badge_LV5")) pic_50WIN.Opacity = 1;
            if (response.ContainsKey("badge_LV10")) pic_100WIN.Opacity = 1;
            if (response.ContainsKey("badge_LV25")) pic_250WIN.Opacity = 1;
            if (response.ContainsKey("badge_champion")) pic_CHAMP.Opacity = 1;

        }

        public void DashboardHandler(Dictionary<String, String> response)
        {
            string idprof = response["idgambar"];
            photo_profpic.Source = new BitmapImage(new Uri("Images/Avatar/" + idprof + ".jpg", UriKind.Relative));
            photo_cover.Source = new BitmapImage(new Uri("Images/Cover/" + idprof + ".jpg", UriKind.Relative));
            label_username.Content = response["nama"];
            label_lokasi.Content = response["lokasi"];
            int experience = int.Parse(response["exp"]);
            progress_exp.Value = experience % 100;
            label_playerLevel.Content = experience / 100;
            label_nextLevel.Content = experience % 100;

            stack_categories.Children.Clear();

            for (int i = 1; response.ContainsKey("kategori" + i); i++)
            {
                Expander expander = new Expander();
                expander.FontSize = 22;
                expander.Foreground = Brushes.White;
                expander.Header = response["kategori" + i];
                expander.Content = new StackPanel();
                expander.Expanded += new RoutedEventHandler(KategoriExpanded);
                stack_categories.Children.Add(expander);
            }

            rankname1.Content = response["rankname1"];
            rankname2.Content = response["rankname2"];
            rankname3.Content = response["rankname3"];
            rankname4.Content = response["rankname4"];
            rankname5.Content = response["rankname5"];

            rankpoint1.Content = response["rankskor1"];
            rankpoint2.Content = response["rankskor2"];
            rankpoint3.Content = response["rankskor3"];
            rankpoint4.Content = response["rankskor4"];
            rankpoint5.Content = response["rankskor5"];

            string idphotorank1 = response["rankgambar1"];
            string idphotorank2 = response["rankgambar2"];
            string idphotorank3 = response["rankgambar3"];
            string idphotorank4 = response["rankgambar4"];
            string idphotorank5 = response["rankgambar5"];
            photo_rank1.Source = new BitmapImage(new Uri("Images/Avatar/" + idphotorank1 + ".jpg", UriKind.Relative));
            photo_rank2.Source = new BitmapImage(new Uri("Images/Avatar/" + idphotorank2 + ".jpg", UriKind.Relative));
            photo_rank3.Source = new BitmapImage(new Uri("Images/Avatar/" + idphotorank3 + ".jpg", UriKind.Relative));
            photo_rank4.Source = new BitmapImage(new Uri("Images/Avatar/" + idphotorank4 + ".jpg", UriKind.Relative));
            photo_rank5.Source = new BitmapImage(new Uri("Images/Avatar/" + idphotorank5 + ".jpg", UriKind.Relative));

            label_jumlahMenang.Content = response["jumlahmenang"];
        }

        private void KategoriExpanded(object sender, EventArgs e)
        {
            String kategori = (sender as Expander).Header.ToString();

            Request request = new Request("dashboard");
            request.Add("filter", "subkategori");
            request.Add("kategori", kategori);
            Server.SendRequest(request, SubKategoriHandler);

            ((sender as Expander).Content as StackPanel).Children.Clear();
        }

        public void SubKategoriHandler(Dictionary<String, String> response)
        {
            foreach (Expander expander in stack_categories.Children)
            {
                if (expander.Header .ToString().Equals(response["kategori"]))
                {
                    for (int i = 1; response.ContainsKey("subkategori" + i); i++)
                    {
                        Button btn = new Button();
                        btn.Height = 30;
                        btn.FontSize = 20;
                        btn.Margin = new Thickness(0,5,0,0);
                        btn.DataContext = response["subkategori" + i];
                        btn.Content = response["subkategori"+i] + " Lv. "+(int.Parse(response["subkategoriexp" + i])/100).ToString();
                        btn.Click += new RoutedEventHandler(btn_SubKategories);
                        ProgressBar btnExp = new ProgressBar();
                        btnExp.Height = 5;
                        btnExp.Value = int.Parse(response["subkategoriexp" + i])%100;
                        (expander.Content as StackPanel).Children.Add(btn);
                        (expander.Content as StackPanel).Children.Add(btnExp);
                    }
                }
            }
        }

        private void btn_SubKategories(object sender, RoutedEventArgs e)
        {
            Request request = new Request("match");
            request.Add("subkategori",(sender as Button).DataContext.ToString());
            request.Add("type", "pool");
            Server.SendRequest(request, MatchPoolWait);

            Dashboard.Visibility = Visibility.Hidden;
            MatchMaking.Visibility = Visibility.Visible;
            loadingDuelTimer = 0;
            image_matchmakingplayer.Source = photo_profpic.Source;
        }

        private void MatchPoolWait(Dictionary<string, string> response)
        {
            if (response["act"].Equals("wait"))
            {
                Request request = new Request("match");
                request.Add("type", "find");
                Server.SendRequest(request, MatchPoolWait);
            }
            else if (response["act"].Equals("duel"))
            {
                Request requestUser = new Request("match");
                requestUser.Add("type", "getuser");
                Server.SendRequest(requestUser, DuelDataUser);

                Request request = new Request("match");
                request.Add("type", "getquestion");
                Server.SendRequest(request, MatchDuel);
            }
        }

        private void DuelDataUser(Dictionary<string, string> response)
        {
            loadingDuelTimer = -1;
            MatchMaking.Visibility = Visibility.Hidden;
            Duel.Visibility = Visibility.Visible;
            duelTimer = 10;
            img_playeravatar.Source = new BitmapImage(new Uri("Images/Avatar/" + response["player1ID"] + ".jpg", UriKind.Relative));
            img_opponentavatar.Source = new BitmapImage(new Uri("Images/Avatar/" + response["player2ID"] + ".jpg", UriKind.Relative));
            img_playercover.Source = new BitmapImage(new Uri("Images/Cover/" + response["player1ID"] + ".jpg", UriKind.Relative));
            img_opponentcover.Source = new BitmapImage(new Uri("Images/Cover/" + response["player2ID"] + ".jpg", UriKind.Relative));
            label_duelplayername.Content = response["nama1"];
            label_duelopponentname.Content = response["nama2"];
        }

        private void MatchDuel(Dictionary<string, string> response)
        {
            if (response.ContainsKey("act") && response["act"].Equals("score"))
            {
                Dashboard.Visibility = Visibility.Visible;
                Duel.Visibility = Visibility.Hidden;

                ScoreBoard.Visibility = Visibility.Visible;
                label_playername.Content = label_duelplayername.Content;
                label_playerscore.Content = pb_playerscore.Value.ToString();
                image_playeravatar.Source = new BitmapImage(new Uri("Images/Avatar/" + response["playerid"] + ".jpg", UriKind.Relative));
                image_playercover.Source = new BitmapImage(new Uri("Images/Cover/" + response["playerid"] + ".jpg", UriKind.Relative));
                label_opponentname.Content = label_duelopponentname.Content;
                label_opponentscore.Content = pb_opponentscore.Value.ToString();
                image_opponentavatar.Source = new BitmapImage(new Uri("Images/Avatar/" + response["opponentid"] + ".jpg", UriKind.Relative));
                image_opponentcover.Source = new BitmapImage(new Uri("Images/Cover/" + response["opponentid"] + ".jpg", UriKind.Relative));
                var bc = new BrushConverter();

                if (pb_playerscore.Value > pb_opponentscore.Value)
                {
                    ScoreBoardDisplay.Background = (Brush)bc.ConvertFrom("#FF41E091");
                }
                else
                {
                    ScoreBoardDisplay.Background = (Brush)bc.ConvertFrom("#FFE04141");
                }

                DashboardDisplay();

                return;
            }

            if (txt_questiondisplay.Text == response["soal"])
            {
                Request request = new Request("match");
                request.Add("type", "getquestion");
                Server.SendRequest(request, MatchDuel);
            }
            else
            {
                duelTimer = 10;
                DuelButtonClear();
                isAnswered = false;

                pb_playerscore.Value = int.Parse(response["skor1"]);
                pb_opponentscore.Value = int.Parse(response["skor2"]);

                txt_questiondisplay.Text = response["soal"];
                txt_questiondisplay.Foreground = Brushes.White;
                button_answeroption1.Content = response["jawaban1"];
                button_answeroption2.Content = response["jawaban2"];
                button_answeroption3.Content = response["jawaban3"];
                button_answeroption4.Content = response["jawaban4"];
            }
        }


        public void RegisterCheck(Dictionary<String, String> response)
        {
            if (response["valid"].Equals("0"))
            {
                AddErrorMessage(response["message"]);
            }
            else
            {
                AddNotificationMessage("Anda berhasi mendaftar");
                Register.Visibility = Visibility.Hidden;
                Login.Visibility = Visibility.Visible;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Request request = new Request("login");
            request.Add("username", txtLogin_Username.Text);
            request.Add("password", txtLogin_Password.Password);
            Server.SendRequest(request, LoginCheck);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Request request = new Request("register");
            request.Add("username_reg", txtRegister_Username.Text);
            request.Add("password_reg", txtRegister_Password.Password);
            request.Add("password_reg_repass", txtRegister_VPassword.Password);
            request.Add("email", txtRegister_Email.Text);
            request.Add("location", txtRegister_Location.Text);
            request.Add("nama", txtRegister_name.Text);
            request.Add("cek", checkBox_Agree.IsChecked.ToString()); 
            Server.SendRequest(request, RegisterCheck);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Login.Visibility = Visibility.Hidden;
            Register.Visibility = Visibility.Visible;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Server.getSession().Clear();
            Login.Visibility = Visibility.Visible;
            Dashboard.Visibility = Visibility.Hidden;
        }

        private void button_signup_cancel_Click_1(object sender, RoutedEventArgs e)
        {
            Register.Visibility = Visibility.Hidden;
            Login.Visibility = Visibility.Visible;
        }

        private void button_answeroption1_Click(object sender, RoutedEventArgs e)
        {
            if (!isAnswered)
            {
                Request request = new Request("match");
                request.Add("type", "giveanswer");
                request.Add("time", duelTimer.ToString());
                request.Add("answer", "1");
                Server.SendRequest(request, MatchAnwer);
                duelTimer = -1;
                isAnswered = true;
            }
        }

        private void button_answeroption2_Click(object sender, RoutedEventArgs e)
        {
            if (!isAnswered)
            {
                Request request = new Request("match");
                request.Add("type", "giveanswer");
                request.Add("time", duelTimer.ToString());
                request.Add("answer", "2");
                Server.SendRequest(request, MatchAnwer);
                duelTimer = -1;
            }
        }

        private void button_answeroption3_Click(object sender, RoutedEventArgs e)
        {
            if (!isAnswered)
            {
                Request request = new Request("match");
                request.Add("type", "giveanswer");
                request.Add("time", duelTimer.ToString());
                request.Add("answer", "3");
                Server.SendRequest(request, MatchAnwer);
                duelTimer = -1;
            }
        }

        private void button_answeroption4_Click(object sender, RoutedEventArgs e)
        {
            if (!isAnswered)
            {
                Request request = new Request("match");
                request.Add("type", "giveanswer");
                request.Add("time", duelTimer.ToString());
                request.Add("answer", "4");
                Server.SendRequest(request, MatchAnwer);
                duelTimer = -1;
            }
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            ScoreBoard.Visibility = Visibility.Hidden;
        }

        private void Window_Unloaded_1(object sender, RoutedEventArgs e)
        {
            Server = null;
        }
    }
}
