﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidRush
{
    class RequestMessage
    {
        public delegate void ResponseHandler(Dictionary<String, String> response);

        public ResponseHandler Handler { set; get; }
        public Request Request { set; get; }
        public String Response { set; get; }
        public bool IsResponded { set; get; }

        public RequestMessage(Request message, ResponseHandler handler)
        {
            Response = "";
            Request = message;
            Handler = handler;

            IsResponded = false;
        }
    }
}
