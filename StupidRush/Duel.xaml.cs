﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace StupidRush
{
    /// <summary>
    /// Interaction logic for Duel.xaml
    /// </summary>
    public partial class Duel : Window
    {
        public Duel()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MainWindow mwindow = new MainWindow(true);
            mwindow.Show();
        }
    }
}
